// Add Record
function addRecord() {
    // get values
    var idalumno = $("#idalumno").val();
    var codalumno = $("#codalumno").val();
    var codmatri = $("#codmatri").val();
    var obs = $("#obs").val();

    // Add record
    $.post("ajax/addRecord.php", {
        idalumno: idalumno,
        codalumno: codalumno,
        codmatri: codmatri,
		obs: obs
    }, function (data, status) {
        // close the popup
        $("#add_new_record_modal").modal("hide");

        // read records again
        readRecords();

        // clear fields from the popup
        $("#idalumno").val("");
        $("#codalumno").val("");
        $("#codmatri").val("");
        $("#obs").val("");
    });
}

// READ records
function readRecords() {
    
    $.get("ajax/readRecord.php", {}, function (data, status) {
        $("#records_content").html(data);
    });
}


function GetUserDetailss(id) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_user_id").val(id);
    $.post("ajax/readUserDetails.php", {
            id: id
        },
        function (data, status) {
            readRecords();
        }
    );
    // Open modal popup
    $("#elimina_user_modal").modal("show");
}


function GetUserDetails(id) {
    // Add User ID to the hidden field for furture usage
    $("#hidden_user_id").val(id);
    $.post("ajax/readUserDetails.php", {
            id: id
        },
        function (data, status) {
            // PARSE json data
            var user = JSON.parse(data);
            // Assing existing values to the modal popup fields
            $("#update_fecha_entrega").val(user.fecha_entrega);
            $("#update_calle").val(user.calle);
            $("#update_no_casa").val(user.no_casa);
            $("#update_localidad").val(user.localidad);
            $("#update_municipio").val(user.municipio);
            $("#update_estado").val(user.estado);
            $("#update_referencia").val(user.referencia);
            $("#update_id_cliente").val(user.id_cliente);
            $("#update_id_arreglo").val(user.id_arreglo);
            $("#useridO").val(user.id_pedido);
        }
    );
    // Open modal popup
    $("#update_user_modal").modal("show");
}



$(document).ready(function () {
    // READ recods on page load
    readRecords(); // calling function
});