<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>Mapa del sitio</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <!-- Start Main Top -->
    <div class="main-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="our-link">
                        <ul>
                            <li><a href="my-account.php"><i class="fa fa-user s_color"></i> Mi cuenta</a></li>
                            <li><a href="shop.php"><i class="fas fa-location-arrow"></i> Mapa del sitio</a></li>
                            <li><a href="contact-us.php"><i class="fas fa-headset"></i> Contactanos</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Top -->

    <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="indexx.php"><img src="../images/logoo.PNG" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item active"><a class="nav-link" href="indexx.php">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="about.php">Sobre Nosotros</a></li>
                        <li class="dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">TIENDA</a>
                            <ul class="dropdown-menu">
                                <li><a href="checkout.php">Realizar Pedido</a></li>
                                <li><a href="my-account.php">Mi Cuenta</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="gallery.php">Arreglos</a></li>
                        <li class="nav-item"><a class="nav-link" href="contact-us.php">Contactanos</a></li>
                    </ul>
                </div>

                <!-- Start Atribute Navigation -->
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Mapa del sitio</h2>
            
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Cart  -->
    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-md-12">
                    <div class="title-left">
                       <ul> <h3>Mapa del sitio</h3></ul>
                    </div>
                        <li class="nav-link"><a href="indexx.php"><h2>Pagina de Inicio</h2></a></li>
                        <li class="nav-link"><a href="about.php"><h2>Pagina de Nosotros</h2></a></li>
                        <li class="nav-link"><a href="checkout.php"><h2>Pagina de Realiza Pedido</h2></a></li>
                        <li class="nav-link"><a href="my-account.php"><h2>Pagina de Mi cuenta</h2></a></li>
                        <li class="nav-link"><a href="gallery.php"><h2>Pagina de Arreglos</h2></a></li>
                        <li class="nav-link"><a href="contact-us.php"><h2>Pagina de Contactanos</h2></a></li> 
                </div>
            </div>
        </div>          
    </div>
    <!-- End Cart -->

     <!-- Start Instagram Feed  -->
    <div class="instagram-box">
        <div class="main-instagram owl-carousel owl-theme">
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-01.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-02.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-03.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-04.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-05.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-06.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-07.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-08.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-09.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-05.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Instagram Feed  -->

     <!-- Start Footer  -->
    <footer>
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-top-box">
                            <h3>Horario de Servicio</h3>
                            <ul class="list-time">
                                <li>Lunes - Viernes: 08.00am to 05.00pm</li> <li>Sabados: 10.00am to 08.00pm</li> <li>Domingo: <span>Cerrado</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-top-box">
                            <h3>Redes Sociales</h3>
                            <br>
                            <ul>
                                <li><a href="https://www.facebook.com/"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-widget">
                            <h4>Sobre Flower Shop</h4>
                            <p>Somos una empresa dedicada a la decoración floral, nos especializamos en el diseño y armado de arreglos florales para cualquier evento social y por también realizamos bellos diseños con flores para transmitir
                            todo tipo de sentimientos humanos.</p> 
                            <p>Queremos lograr ser una florería que se distinga de las demás en el diseño de arreglos florales y decoración de eventos, siendo nuestro principal objetivo la satisfacción de nuestros clientes en especial.</p>  
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link-contact">
                            <h4>Contáctate con Nosotros</h4>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Dirección: San Carlos<br>Calle principal,<br> numero 18 </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>Teléfono: <a href="tel:+52-57872075">+52-57872075</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:contactinfo@gmail.com">flowershop@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer  -->

    <!-- Start copyright  -->
    <div class="footer-copyright">
        <p class="footer-company">Todos los derechos reservados. &copy; 2020 <a href="indexx.php">FlowerShop</a> Diseñado por :
            <a href="https://html.design/">diseño HTML</a></p>
    </div>
    <!-- End copyright  -->

    <a href="indexx.php" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="../js/jquery.superslides.min.js"></script>
    <script src="../js/bootstrap-select.js"></script>
    <script src="../js/inewsticker.js"></script>
    <script src="../js/bootsnav.js."></script>
    <script src="../js/images-loded.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/baguetteBox.min.js"></script>
    <script src="../js/form-validator.min.js"></script>
    <script src="../js/contact-form-script.js"></script>
    <script src="../js/custom.js"></script> 
    <script type="text/javascript" src="../js/script.js"></script> 
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
</body>

</html>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btnguardar').click(function(){
        var datos=$('#form_pedido').serialize();
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Pedido&action=create",
                data:datos,
                dataType:'json',
                success:function(r){
                    if (r.estatus==="success") {
                        alert("Pedido registrado con exito");
                         $("#btncancelar").click();
                        readRecords();
                    }
                    else{
                        alert("no agregado!");
                    }
                }
            });
            return false;
        });
    });
    function readRecords() {
    $.get("ajax/readRecord.php", {}, function (data, status) {
        $("#records_content").html(data);
    });
    }

    $(document).ready(function(){
        $('#btneditar').click(function(){
        var datos=$('#form_editpedido').serialize();
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Pedido&action=update",
                data:datos,
                dataType:'json',
                success:function(r){
                    if (r.estatus==="success") {
                        alert("Pedido modificado con exito");
                         $("#btncancelar2").click();
                        readRecords();
                    }
                    else{
                        alert("no modificado!");
                    }
                }
            });
            return false;
        });
    });

    $(document).ready(function(){
        $('#btnaceptar').click(function(){
        var datos=$('#form_elimina').serialize();
        alert(datos);
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Pedido&action=delete",
                data:datos,
                success:function(r){
                         $("#btncancela").click();
                        readRecords();
                }
            });
            return false;
        });
    });

 $(document).ready(function(){
        $('#btnregistrar_cliente').click(function(){
        var datos=$('#formRegister').serialize();
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Cliente&action=create",
                data:datos,
                dataType:'json',
                success:function(r){
                    if (r.estatus==="success") {
                        alert("Se registro con exito, ya puedes realizar tu pedido");
                    }
                    else{
                        alert("no agregado!");
                    }
                }
            });
            return false;
        });
    });


$(document).ready(function () {
    readRecords(); 
});

</script>