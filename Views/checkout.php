<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>Realizar Pedido en Flower Shop</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <!-- Start Main Top -->
    <div class="main-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="our-link">
                        <ul>
                            <li><a href="my-account.php"><i class="fa fa-user s_color"></i> Mi cuenta</a></li>
                            <li><a href="shop.php"><i class="fas fa-location-arrow"></i> Mapa del sitio</a></li>
                            <li><a href="contact-us.php"><i class="fas fa-headset"></i> Contactanos</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Top -->

    <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="indexx.php"><img src="../images/logoo.PNG" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item active"><a class="nav-link" href="indexx.php">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="about.php">Sobre Nosotros</a></li>
                        <li class="dropdown">
                            <a href="indexx.php" class="nav-link dropdown-toggle" data-toggle="dropdown">Inicio</a>
                            <ul class="dropdown-menu">
                                <li><a href="checkout.php">Realizar Pedido</a></li>
                                <li><a href="my-account.php">Mi Cuenta</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="gallery.php">Arreglos</a></li>
                        <li class="nav-item"><a class="nav-link" href="contact-us.php">Contactanos</a></li>
                    </ul>
                </div>

        </nav>
        <!-- End Navigation -->
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Realizar Pedido</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="indexx.php">Inicio</a></li>
                        <li class="breadcrumb-item active">Realizar Pedido</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Cart  -->
    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-6 col-lg-6 mb-3">
                    <div class="title-left">
                        <h3>Crear una Nueva Cuenta</h3>
                    </div>
                    <h5><a data-toggle="collapse" href="#formRegister" role="button" aria-expanded="false">Pulse aquí para registrarse</a></h5>
                    <form class="mt-3 collapse review-form-box" id="formRegister" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="InputName" class="mb-0">Nombre:</label>
                                <input type="text" class="form-control" name="nombre" id="InputName" placeholder="Nombre (S) "> </div>
                            <div class="form-group col-md-6">
                                <label for="InputLastname" class="mb-0">Apellido paterno:</label>
                                <input type="text" class="form-control" name="apellido_p" id="InputLastname" placeholder="Apellido paterno"> </div>
                             <div class="form-group col-md-6">
                                <label for="InputLastname" class="mb-0">Apellido materno:</label>
                                <input type="text" class="form-control" name="apellido_m" id="InputLastname" placeholder="Apellido materno"></div>
                             <div class="form-group col-md-6">
                                <label for="InputLastname" class="mb-0"># Casa:</label>
                                <input type="text" class="form-control" name="no_casa" id="InputLastname" placeholder="#14"> </div>

                             <div class="form-group col-md-6">
                                <label for="InputLastname" class="mb-0">calle:</label>
                                <input type="text" class="form-control" name="calle" id="InputLastname" placeholder="Calle"> </div>
                            <div class="form-group col-md-6">
                                <label for="InputEmail1" class="mb-0">Localidad:</label>
                                <input type="text" class="form-control" name="localidad" id="InputEmail1" placeholder="Localidad"> </div>
                             <div class="form-group col-md-6">
                                <label for="InputEmail1" class="mb-0">Municipio:</label>
                                <input type="text" class="form-control" name="municipio" id="InputEmail1" placeholder="Municipio"> </div>
                             <div class="form-group col-md-6">
                                <label for="InputEmail1" class="mb-0">Estado:</label>
                                <input type="text" class="form-control" name="estado" id="InputEmail1" placeholder="EstadoEstado"> </div>
                            <div class="form-group col-md-6">
                                <label for="InputPassword1" class="mb-0">Referencia:</label>
                                <input type="text" class="form-control" name="referencia" id="InputPassword1" placeholder="Referencia"> </div>
                        </div>
                        <button type="submit" class="btn hvr-hover" id="btnregistrar_cliente">Registrar</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-6 mb-3">
                    <div class="row">                       
                        <div class="col-12 d-flex shopping-box"> <a href="checkout.php" class="ml-auto btn hvr-hover" data-toggle="modal" data-target="#add_new_record_modal">Realizar Pedido</a> </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                <div class="table-main table-responsive" id="records_content"></div>
                </div>
            </div>
            
            <!-- Modal - Add New Record/User -->
<div class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
   
      <div class="modal-header">
        <h5 class="modal-title">Agregar Pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
      <div class="modal-body">
        <div class="form-group">
        <form id="form_pedido" method="POST" >
          <label for="fecha">Fecha de entrega</label>
          <input type="date" name="fecha_entrega" id="fecha_entrega" value=""   class="form-control"/>
        </div>
        <hr>
        <h5>Direccion </h5>
        <div class="form-group">
          <label for="calle">Calle</label>
          <input type="text" name="calle" id="calle" class="form-control" value=""/>
        </div>
        <div class="form-group">
          <label for="numero">Numero de casa</label>
          <input type="text" name="no_casa" id="no_casa" value=""   class="form-control"/>
        </div>
        <div class="form-group">
          <label for="fecha">Localidad</label>
          <input type="text" name="localidad" id="localidad" value=""   class="form-control"/>
        </div>
        <div class="form-group">
          <label for="municipio">Municipio</label>
          <input type="text" name="municipio" id="municipio" value=""   class="form-control"/>
        </div>
        <div class="form-group">
          <label for="estado">Estado</label>
          <input type="text" name="estado" id="municipio" value=""   class="form-control"/>
        </div>
        <div class="form-group">
          <label for="referencia">Referencia</label>
           <textarea  id="Referencia"  name="referencia"class="form-control"></textarea>
        </div>
        <div class="form-group">
          <label for="id_cliente">Ingresa tu id cliente</label>
           <input type="text" name="id_cliente" id="id_cliente" value=""   class="form-control"/>
        </div>
        <div class="form-group">
          <label for="id_arreglo">Ingresa id arreglo</label>
          <input type="text" name="id_arreglo" id="id_arreglo" value=""   class="form-control"/>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btncancelar" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnguardar">Agregar</button>
      </div>
    </div>
  </div>
</div>
<!-- // Modal -->
<div class="modal fade" id="elimina_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">¿Estas seguro de eliminar este pedido?</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form id="form_elimina" method="POST" >
            <input type="hidden" name="id_pedido" id="hidden_user_id">
        </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btncancela" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnaceptar">Aceptar</button>
      </div>
  </div>
</div>
</div>
<!-- Modal - Update User Actualizar Usuario -->
<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
   
      <div class="modal-header">
        <h5 class="modal-title">Actualizar Pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> 
      
      <div  class="modal-body">
        <div class="form-group">
            <form id="form_editpedido" method="POST">
         <label for="fecha">Fecha de entrega</label>
          <input type="date" id="update_fecha_entrega" name="fecha_entrega" placeholder="Last Name" class="form-control"/>
        </div>
         <hr>
        <h5>Direccion </h5>
        <div class="form-group">
          <label for="calle">Calle</label>
          <input type="text" id="update_calle" name="calle" placeholder="Last Name" class="form-control"/>
        </div>
         <div class="form-group">
            <label for="numero">Numero de casa</label>
          <input type="text" id="update_no_casa" name="no_casa" placeholder="Last Name" class="form-control"/>
        </div>
         <div class="form-group">
            <label for="fecha">Localidad</label>
          <input type="text" id="update_localidad" name="localidad" placeholder="Last Name" class="form-control"/>
        </div>
         <div class="form-group">
          <label for="municipio">Municipio</label>
          <input type="text" id="update_municipio" name="municipio" placeholder="Last Name" class="form-control"/>
        </div>
        <div class="form-group">
          <label for="estado">Estado</label>
          <input type="text" id="update_estado" name="estado" placeholder="Last Name" class="form-control"/>
        </div>
        <div class="form-group">
          <label for="municipio">Referencia</label>
          <textarea  id="update_referencia" name="referencia" placeholder="Last Name" class="form-control"></textarea>
        </div>
        <div class="form-group">
         <label for="id_cliente">Ingresa tu id cliente</label>
          <input type="text" id="update_id_cliente" name="id_cliente" placeholder="Last Name" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="id_arreglo">Ingresa id arreglo</label>
            <input type="text" id="update_id_arreglo" name="id_arreglo" placeholder="Last Name" class="form-control"/>
        </div>
        <div class="form-group">
            <input type="hidden" name="id_pedido" id="useridO">
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btncancelar2" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btneditar">Guardar Cambios</button>
        
      </div>
    </div>
  </div>
</div>

        </div>
    </div>
    <!-- End Cart -->

     <!-- Start Instagram Feed  -->
    <div class="instagram-box">
        <div class="main-instagram owl-carousel owl-theme">
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-01.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-02.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-03.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-04.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-05.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-06.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-07.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-08.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-09.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="ins-inner-box">
                    <img src="../images/instagram-img-05.jpg" alt="" />
                    <div class="hov-in">
                        <a href="https://www.instagram.com"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Instagram Feed  -->

     <!-- Start Footer  -->
    <footer>
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-top-box">
                            <h3>Horario de Servicio</h3>
                            <ul class="list-time">
                                <li>Lunes - Viernes: 08.00am to 05.00pm</li> <li>Sabados: 10.00am to 08.00pm</li> <li>Domingo: <span>Cerrado</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-top-box">
                            <h3>Redes Sociales</h3>
                            <br>
                            <ul>
                                <li><a href="https://www.facebook.com/"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-widget">
                            <h4>Sobre Flower Shop</h4>
                            <p>Somos una empresa dedicada a la decoración floral, nos especializamos en el diseño y armado de arreglos florales para cualquier evento social y por también realizamos bellos diseños con flores para transmitir
                            todo tipo de sentimientos humanos.</p> 
                            <p>Queremos lograr ser una florería que se distinga de las demás en el diseño de arreglos florales y decoración de eventos, siendo nuestro principal objetivo la satisfacción de nuestros clientes en especial.</p>  
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link-contact">
                            <h4>Contáctate con Nosotros</h4>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Dirección: San Carlos<br>Calle principal,<br> numero 18 </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>Teléfono: <a href="tel:+52-57872075">+52-57872075</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:contactinfo@gmail.com">flowershop@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer  -->

    <!-- Start copyright  -->
    <div class="footer-copyright">
        <p class="footer-company">Todos los derechos reservados. &copy; 2020 <a href="indexx.php">FlowerShop</a> Diseñado por :
            <a href="https://html.design/">diseño HTML</a></p>
    </div>
    <!-- End copyright  -->

    <a href="indexx.php" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="../js/jquery.superslides.min.js"></script>
    <script src="../js/bootstrap-select.js"></script>
    <script src="../js/inewsticker.js"></script>
    <script src="../js/bootsnav.js."></script>
    <script src="../js/images-loded.min.js"></script>
    <script src="../js/isotope.min.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/baguetteBox.min.js"></script>
    <script src="../js/form-validator.min.js"></script>
    <script src="../js/contact-form-script.js"></script>
    <script src="../js/custom.js"></script> 
    <script type="text/javascript" src="../js/script.js"></script> 
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
</body>

</html>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btnguardar').click(function(){
        var datos=$('#form_pedido').serialize();
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Pedido&action=create",
                data:datos,
                dataType:'json',
                success:function(r){
                    if (r.estatus==="success") {
                        alert("Pedido registrado con exito");
                         $("#btncancelar").click();
                        readRecords();
                    }
                    else{
                        alert("no agregado!");
                    }
                }
            });
            return false;
        });
    });

    function readRecords() {
    $.get("ajax/readRecord.php", {}, function (data, status) {
        $("#records_content").html(data);
    });
    }

    $(document).ready(function(){
        $('#btneditar').click(function(){
        var datos=$('#form_editpedido').serialize();
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Pedido&action=update",
                data:datos,
                dataType:'json',
                success:function(r){
                    if (r.estatus==="success") {
                        alert("Pedido modificado con exito");
                         $("#btncancelar2").click();
                        readRecords();
                    }
                    else{
                        alert("no modificado!");
                    }
                }
            });
            return false;
        });
    });

    $(document).ready(function(){
        $('#btnaceptar').click(function(){
        var datos=$('#form_elimina').serialize();
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Pedido&action=delete",
                data:datos,
                success:function(r){
                         $("#btncancela").click();
                        readRecords();
                }
            });
            return false;
        });
    });

 $(document).ready(function(){
        $('#btnregistrar_cliente').click(function(){
        var datos=$('#formRegister').serialize();
            $.ajax({
                type:"POST",
                url:"http://localhost/Csn/freshshop/index.php?controller=Cliente&action=create",
                data:datos,
                dataType:'json',
                success:function(r){
                    if (r.estatus==="success") {
                        alert("Se registro con exito, ya puedes realizar tu pedido");
                    }
                    else{
                        alert("no agregado!");
                    }
                }
            });
            return false;
        });
    });


$(document).ready(function () {
    readRecords(); 
});

</script>