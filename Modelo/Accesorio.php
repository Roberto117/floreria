<?php 
	namespace Trabajo\Modelo;

	class Accesorio extends Conexion{
		public $nom_accesorio;
		public $cantidad;
		public $precio;
		public $tipo;
		public $tamannio;
		public $color;

		function insert(){
			$pre = mysqli_prepare($this->con, "INSERT INTO accesorio(nom_accesorio, cantidad, precio, tipo, tamannio, color) VALUES (?,?,?,?,?,?)");
			$pre->bind_param("sidsss", $this->nom_accesorio, $this->cantidad, $this->precio, $this->tipo, $this->tamannio, $this->color);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}

		static function selectAll(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM accesorio");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Accesorio::class);
		}

		static function find($nom_accesorio){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM accesorio WHERE nom_accesorio=?");
			$pre->bind_param("s", $nom_accesorio);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Accesorio::class);
		}

		static function findColor($color){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM accesorio WHERE color=?");
			$pre->bind_param("s", $color);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Accesorio::class);
		}

		public function editar(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "UPDATE accesorio SET cantidad=?, precio=?, tipo=?, tamannio=?, color=? WHERE nom_accesorio=?");

			$pre->bind_param("idssss",$this->cantidad, $this->precio, $this->tipo, $this->tamannio, $this->color, $this->nom_accesorio);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}
		static function eliminar($nom_accesorio){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "DELETE  FROM accesorio WHERE nom_accesorio=?");
			$pre->bind_param("s", $nom_accesorio);
			$pre->execute();
		}
	}

?>