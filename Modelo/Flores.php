<?php 
namespace Trabajo\Modelo;

	class Flores extends Conexion{
		public $nombre;
		public $precio;
		public $id_proveedor;
		public $nom_especie;
		public $id_arreglo;

		function insert(){
			$pre = mysqli_prepare($this->con, "INSERT INTO flores(nombre, precio, id_proveedor, nom_especie, id_arreglo) VALUES (?,?,?,?,?)");
			$pre->bind_param("sdisi", $this->nombre, $this->precio, $this->id_proveedor, $this->nom_especie, $this->id_arreglo);
			$pre->execute();

			//buena practica:
			$pre_=mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id_flor");
			$pre_->execute();

			$r = $pre_->get_result();
			$this->id_flor = $r->fetch_assoc() ["id_flor"];

			return true;
		}

		static function selectAll(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM flores");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Flores::class);
		}

		static function find($id_flor){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM flores WHERE id_flor=?");
			$pre->bind_param("i", $id_flor);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Flores::class);
		}

		static function findNombre($nombre){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM flores WHERE nombre=?");
			$pre->bind_param("s", $nombre);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Flores::class);
		}

		public function editar(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "UPDATE flores SET nombre=?, precio=?, id_proveedor=?, nom_especie=?, id_arreglo=? WHERE id_flor=?");

			$pre->bind_param("sdisii",$this->nombre, $this->precio, $this->id_proveedor, $this->nom_especie, $this->id_arreglo, $this->id_flor);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}
		static function eliminar($id_flor){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "DELETE  FROM flores WHERE id_flor=?");
			$pre->bind_param("i", $id_flor);
			$pre->execute();
		}
	}

?>