<?php 
namespace Trabajo\Modelo;

	class Cliente extends Conexion{
		public $nombre;
		public $apellido_p;
		public $apellido_m;
		public $calle;
		public $no_casa;
		public $localidad;
		public $municipio;
		public $estado;
		public $referencia;

		function insert(){
			$pre = mysqli_prepare($this->con, "INSERT INTO cliente(nombre, apellido_p, apellido_m, calle, no_casa, localidad, municipio, estado, referencia) VALUES (?,?,?,?,?,?,?,?,?)");
			$pre->bind_param("ssssissss", $this->nombre, $this->apellido_p, $this->apellido_m, $this->calle, $this->no_casa, $this->localidad, $this->municipio, $this->estado, $this->referencia);
			$pre->execute();

			//buena practica:
			$pre_=mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id_cliente");
			$pre_->execute();

			$r = $pre_->get_result();
			$this->id_cliente = $r->fetch_assoc() ["id_cliente"];

			return true;
		}

		static function selectAll(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM cliente");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Cliente::class);
		}

		static function find($id_cliente){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM cliente WHERE id_cliente=?");
			$pre->bind_param("i", $id_cliente);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Cliente::class);
		}

		static function findNombre($nombre){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM cliente WHERE nombre=?");
			$pre->bind_param("s", $nombre);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Cliente::class);
		}

		public function editar(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "UPDATE cliente SET nombre=?, apellido_p=?, apellido_m=?, calle=?, no_casa=?, localidad=?, municipio=?, estado=?, referencia=? WHERE id_cliente=?");

			$pre->bind_param("ssssissssi",$this->nombre, $this->apellido_p, $this->apellido_m, $this->calle, $this->no_casa, $this->localidad, $this->municipio, $this->estado, $this->referencia,$this->id_cliente);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}
		static function eliminar($id_cliente){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "DELETE  FROM cliente WHERE id_cliente=?");
			$pre->bind_param("i", $id_cliente);
			$pre->execute();
		}
	}

?>