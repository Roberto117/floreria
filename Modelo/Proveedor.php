<?php 
namespace Trabajo\Modelo;

	class Proveedor extends Conexion{
		public $nombre;
		public $direccion;
		public $telefono;
		public $email;

		function insert(){
			$pre = mysqli_prepare($this->con, "INSERT INTO proveedor(nombre, direccion, telefono, email) VALUES (?,?,?,?)");
			$pre->bind_param("ssss", $this->nombre, $this->direccion, $this->telefono, $this->email);
			$pre->execute();

			//buena practica:
			$pre_=mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id_proveedor");
			$pre_->execute();

			$r = $pre_->get_result();
			$this->id_proveedor = $r->fetch_assoc() ["id_proveedor"];

			return true;
		}

		static function selectAll(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM proveedor");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Proveedor::class);
		}

		static function find($id_proveedor){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM proveedor WHERE id_proveedor=?");
			$pre->bind_param("i", $id_proveedor);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Proveedor::class);
		}

		static function findNombre($nombre){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM proveedor WHERE nombre=?");
			$pre->bind_param("s", $nombre);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Proveedor::class);
		}

		public function editar(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "UPDATE proveedor SET nombre=?, direccion=?, telefono=?, email=? WHERE id_proveedor=?");

			$pre->bind_param("ssssi",$this->nombre, $this->direccion, $this->telefono, $this->email, $this->id_proveedor);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}
		static function eliminar($id_proveedor){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "DELETE  FROM proveedor WHERE id_proveedor=?");
			$pre->bind_param("i", $id_proveedor);
			$pre->execute();
		}
	}

?>