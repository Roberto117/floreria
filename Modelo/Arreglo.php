<?php 
namespace Trabajo\Modelo;

	class Arreglo extends Conexion{
		public $id_arreglo;
		public $nombre;
		public $modelo;
		public $descripcion;
		public $precio;
		public $nom_accesorio;

		function insert(){
			$pre = mysqli_prepare($this->con, "INSERT INTO arreglo (nombre, modelo, descripcion, precio, nom_accesorio) VALUES (?,?,?,?,?)");
			$pre->bind_param("sssds", $this->nombre, $this->modelo, $this->descripcion, $this->precio, $this->nom_accesorio);
			$pre->execute();

			//buena practica:
			$pre_=mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id_arreglo");
			$pre_->execute();

			$r = $pre_->get_result();
			$this->id_arreglo = $r->fetch_assoc() ["id_arreglo"];

			return true;
		}

		static function selectAll(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM arreglo");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Arreglo::class);
		}

		static function find($id_arreglo){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM arreglo WHERE id_arreglo=?");
			$pre->bind_param("i", $id_arreglo);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Arreglo::class);
		}

		static function findNombre($nombre){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM arreglo WHERE nombre=?");
			$pre->bind_param("s", $nombre);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Arreglo::class);
		}

		public function editar(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "UPDATE arreglo SET nombre=?, modelo=?, descripcion=?, precio=?, nom_accesorio=? WHERE id_arreglo=?");

			$pre->bind_param("sssdsi", $this->nombre, $this->modelo, $this->descripcion, $this->precio, $this->nom_accesorio, $this->id_arreglo);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}
		static function eliminar($id_arreglo){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "DELETE  FROM arreglo WHERE id_arreglo=?");
			$pre->bind_param("i", $id_arreglo);
			$pre->execute();
		}
	}

?>