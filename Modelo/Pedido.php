<?php 
namespace Trabajo\Modelo;

	class Pedido extends Conexion{
		public $fecha_entrega;
		public $calle;
		public $no_casa;
		public $localidad;
		public $municipio;
		public $estado;
		public $referencia;
		public $id_cliente;
		public $id_arreglo;

		function insert(){
			$pre = mysqli_prepare($this->con, "INSERT INTO pedido (fecha, fecha_entrega, calle, no_casa, localidad, municipio, estado, referencia, id_cliente, id_arreglo) VALUES (?,?,?,?,?,?,?,?,?,?)");
			//var_dump($this);
			$fechact=date("Y-m-d");
			$pre->bind_param("sssissssii",$fechact, $this->fecha_entrega, $this->calle, $this->no_casa, $this->localidad, $this->municipio, $this->estado, $this->referencia, $this->id_cliente, $this->id_arreglo);

			$pre->execute();

			//buena practica:
			$pre_=mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id_pedido");
			$pre_->execute();

			$r = $pre_->get_result();
			$this->id_pedido = $r->fetch_assoc() ["id_pedido"];



			return true;
		}

		static function selectAll(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM pedido");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Pedido::class);
		}

		static function find($id_pedido){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM pedido WHERE id_pedido=?");
			$pre->bind_param("i", $id_pedido);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Pedido::class);
		}

		static function findFecha($fecha){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM pedido WHERE fecha=?");
			$pre->bind_param("s", $fecha);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Pedido::class);
		}

		public function editar(){
			$me = new Conexion();

			$pre = mysqli_prepare($me->con, "UPDATE pedido SET fecha=?, fecha_entrega=?, calle=?, no_casa=?, localidad=?, municipio=?, estado=?, referencia=?, id_cliente=?, id_arreglo=? WHERE id_pedido=?");
			$fechact=date("Y-m-d");
			$pre->bind_param("sssissssiii", $fechact, $this->fecha_entrega, $this->calle, $this->no_casa, $this->localidad, $this->municipio, $this->estado, $this->referencia, $this->id_cliente, $this->id_arreglo, $this->id_pedido);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}
		static function eliminar($id_pedido){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "DELETE  FROM pedido WHERE id_pedido=?");
			$pre->bind_param("i", $id_pedido);
			$pre->execute();
		}
	}

?>