<?php 
namespace Trabajo\Modelo;

	class Especie extends Conexion{
		public $nom_especie;
		public $floracion;

		function insert(){
			$pre = mysqli_prepare($this->con, "INSERT INTO especie(nom_especie, floracion) VALUES (?,?)");
			$pre->bind_param("ss", $this->nom_especie, $this->floracion);
			$pre->execute();

			//buena practica:
			$pre_=mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() nom_especie");
			$pre_->execute();

			$r = $pre_->get_result();
			$this->nom_especie = $r->fetch_assoc() ["nom_especie"];

			return true;
		}

		static function selectAll(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM especie");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Especie::class);
		}

		static function find($nom_especie){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM especie WHERE nom_especie=?");
			$pre->bind_param("s", $nom_especie);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Especie::class);
		}

		static function findNombre($floracion){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "SELECT * FROM especie WHERE floracion=?");
			$pre->bind_param("s", $floracion);
			$pre->execute();
			$res = $pre->get_result();
			return $res->fetch_object(Especie::class);
		}

		public function editar(){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "UPDATE especie SET floracion=? WHERE nom_especie=?");

			$pre->bind_param("ss",$this->floracion, $this->nom_especie);
			$pre->execute();
			$r = $pre->get_result();
			return true;
		}
		static function eliminar($nom_especie){
			$me = new Conexion();
			$pre = mysqli_prepare($me->con, "DELETE  FROM especie WHERE nom_especie=?");
			$pre->bind_param("s", $nom_especie);
			$pre->execute();
		}
	}

?>