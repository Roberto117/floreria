-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-03-2020 a las 19:54:52
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `floreria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesorio`
--

CREATE TABLE `accesorio` (
  `nom_accesorio` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` float(8,2) NOT NULL,
  `tipo` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tamannio` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `color` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `accesorio`
--

INSERT INTO `accesorio` (`nom_accesorio`, `cantidad`, `precio`, `tipo`, `tamannio`, `color`) VALUES
('moño', 2, 20.00, 'color', 'Mediano', 'rojo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arreglo`
--

CREATE TABLE `arreglo` (
  `id_arreglo` int(11) NOT NULL,
  `nombre` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL,
  `modelo` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `descripcion` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL,
  `precio` float(8,2) NOT NULL,
  `nom_accesorio` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `arreglo`
--

INSERT INTO `arreglo` (`id_arreglo`, `nombre`, `modelo`, `descripcion`, `precio`, `nom_accesorio`) VALUES
(1, 'Canasta', 'Md52', 'Canasta con girasoles', 300.00, 'moño');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellido_p` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellido_m` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `calle` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `no_casa` int(11) NOT NULL,
  `localidad` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `municipio` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `referencia` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `apellido_p`, `apellido_m`, `calle`, `no_casa`, `localidad`, `municipio`, `estado`, `referencia`) VALUES
(1, 'Fatima', 'Flores', 'Luis', 'Baical', 23, 'urbi', 'Tecamac', 'Edomex', 'Bodega'),
(3, 'Jesus', 'Flores', 'Morales', 'Norte 10', 8, 'san carlos', 'ecatepec', 'Edomex', 'tienda'),
(4, 'Juanito', 'mora', 'ruiz', 'lago', 3, 'zumpango', 'tecamac', 'mex', 'local de frutas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especie`
--

CREATE TABLE `especie` (
  `nom_especie` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `floracion` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `especie`
--

INSERT INTO `especie` (`nom_especie`, `floracion`) VALUES
('Girasol', 'mayo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `flores`
--

CREATE TABLE `flores` (
  `id_flor` int(11) NOT NULL,
  `nombre` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL,
  `precio` float(8,2) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `nom_especie` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `id_arreglo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `id_pedido` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `fecha_entrega` date NOT NULL,
  `calle` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `no_casa` int(11) NOT NULL,
  `localidad` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `municipio` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `referencia` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_arreglo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id_pedido`, `fecha`, `fecha_entrega`, `calle`, `no_casa`, `localidad`, `municipio`, `estado`, `referencia`, `id_cliente`, `id_arreglo`) VALUES
(1, '2020-03-17', '2020-03-19', 'Baykal', 14, 'ojo de agua', 'Tecamac', 'Mexico', 'porton negro', 1, 1),
(2, '2020-03-19', '2020-03-20', 'Granadas', 54, 'Reyes', 'Tecamac', 'MEX', 'casa rosa', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `direccion` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL,
  `telefono` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `e-mail` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre`, `direccion`, `telefono`, `e-mail`) VALUES
(1, 'juan', 'Rio Duero # 14', '5533041371', 'juan@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesorio`
--
ALTER TABLE `accesorio`
  ADD PRIMARY KEY (`nom_accesorio`);

--
-- Indices de la tabla `arreglo`
--
ALTER TABLE `arreglo`
  ADD PRIMARY KEY (`id_arreglo`),
  ADD KEY `nom_accesorio` (`nom_accesorio`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `especie`
--
ALTER TABLE `especie`
  ADD PRIMARY KEY (`nom_especie`);

--
-- Indices de la tabla `flores`
--
ALTER TABLE `flores`
  ADD PRIMARY KEY (`id_flor`),
  ADD KEY `nom_especie` (`nom_especie`),
  ADD KEY `id_proveedor` (`id_proveedor`),
  ADD KEY `id_arreglo` (`id_arreglo`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_arreglo` (`id_arreglo`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `arreglo`
--
ALTER TABLE `arreglo`
  MODIFY `id_arreglo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `flores`
--
ALTER TABLE `flores`
  MODIFY `id_flor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `arreglo`
--
ALTER TABLE `arreglo`
  ADD CONSTRAINT `arreglo_ibfk_1` FOREIGN KEY (`nom_accesorio`) REFERENCES `accesorio` (`nom_accesorio`);

--
-- Filtros para la tabla `flores`
--
ALTER TABLE `flores`
  ADD CONSTRAINT `flores_ibfk_1` FOREIGN KEY (`nom_especie`) REFERENCES `especie` (`nom_especie`),
  ADD CONSTRAINT `flores_ibfk_2` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedor` (`id_proveedor`),
  ADD CONSTRAINT `flores_ibfk_3` FOREIGN KEY (`id_arreglo`) REFERENCES `arreglo` (`id_arreglo`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`id_arreglo`) REFERENCES `arreglo` (`id_arreglo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
