<?php
	
	include("db_connection.php");

	
	$data = '<table class="table">
					<thead>
						<tr>
							<th>No. de<br>Pedido</th>
							<th>Fecha</th>
							<th>Fecha<br>Entrega</th>
							<th>Calle</th>
							<th> # Casa</th>
							<th>Localidad</th>
							<th>Municipio</th>
							<th>Estado</th>
							<th>Referencia</th>
							<th># Cliente</th>
							<th># Arreglo</th>
							<th colspan="2">Acciones</th>
						</tr>
					</thead>';

	$query = "SELECT * FROM pedido";

	if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con));
    }

   
    if(mysqli_num_rows($result) > 0)
    {
    	$number = 1;
    	while($row = mysqli_fetch_assoc($result))
    	{
    		$data .= '<tr>
				<td>'.$row['id_pedido'].'</td>
				<td>'.$row['fecha'].'</td>
				<td>'.$row['fecha_entrega'].'</td>
				<td>'.$row['calle'].'</td>
				<td>'.$row['no_casa'].'</td>
				<td>'.$row['localidad'].'</td>
				<td>'.$row['municipio'].'</td>
				<td>'.$row['estado'].'</td>
				<td>'.$row['referencia'].'</td>
				<td>'.$row['id_cliente'].'</td>
				<td>'.$row['id_arreglo'].'</td>
				<td>
					<button onclick="GetUserDetails('.$row['id_pedido'].')" class="btn hvr-hover" data-toggle="modal" data-target="#update_user_modal"><i class="fas fa-edit"></i></button>
				</td>
				<td>
					<button onclick="GetUserDetailss('.$row['id_pedido'].')" class="btn hvr-hover" data-toggle="modal" data-target="#elimina_user_modal"
					><i class="far fa-trash-alt"></i></button>
				</td>
    		</tr>';
    		$number++;
    	}
    }
    else
    {
    	// records now found 
    	$data .= '<tr><td colspan="6">No hay registros!</td></tr>';
    }

    $data .= '</table>';

    echo $data;
?>