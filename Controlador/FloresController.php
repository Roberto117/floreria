<?php 
use Trabajo\Modelo\Flores;

include "Modelo\Conexion.php";
include "Modelo\Flores.php";

	class FloresController
	{
		
		function __construct()
		{
			$this-> controller = "Flores";
			$this-> action = "create";
		}

		public function create(){
			
			if(isset($_POST)){
			$flores = new Flores();
			$flores->nombre = $_POST["nombre"];
			$flores->precio= $_POST["precio"];
			$flores->id_proveedor = $_POST["id_proveedor"];
			$flores->nom_especie = $_POST["nom_especie"];
			$flores->id_arreglo = $_POST["id_arreglo"];
			
			$flores->insert();

			echo json_encode(["estatus"=>"success", "flores"=>$flores]);
			}
			else{
				echo "Error! revisa los campos";
			}
		}

		public function read(){
			$flores = Flores::selectAll();
			var_dump($flores);
		}

		public function showid(){
			if (isset($_POST)) {
				$id_flor=$_POST["id_flor"];
				$flores=Flores::find($id_flor);
				var_dump($flores);
			}
			else{
				echo "Error! ingresa un id_flor";
			}
			
		}

		public function showNom(){
			if (isset($_POST)) {
				$nombre=$_POST["nombre"];
				$flores=Flores::findNombre($nombre);
				var_dump($flores);
			}
			else{
				echo "Error! ingresa campo nombre";
			}	
		}
		public function update(){
			if (isset($_POST)) {
			$flores = new Flores();

			$flores->nombre = $_POST["nombre"];
			$flores->precio = $_POST["precio"];
			$flores->id_proveedor = $_POST["id_proveedor"];
			$flores->nom_especie = $_POST["nom_especie"];
			$flores->id_arreglo = $_POST["id_arreglo"];
			$flores->id_flor = $_POST["id_flor"];

			$flores->editar();
			echo json_encode(["estatus"=>"success", "flores"=>$flores]);
			}
			else{
				echo "Error! Revisa los campos";
			}
		}

		public function delete(){
			if (isset($_POST)) {
				$id_flor=$_POST["id_flor"];
				$flores=Flores::eliminar($id_flor);
			}
			else{
				echo "No se pudo eliminar, se necesita un id";
			}
			
		}
	}

?>