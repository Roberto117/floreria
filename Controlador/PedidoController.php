<?php 
use Trabajo\Modelo\Pedido;

include "Modelo\Conexion.php";
include "Modelo\Pedido.php";

	class PedidoController
	{
		
		function __construct()
		{
			$this-> controller = "Pedido";
			$this-> action = "create";
		}

		public function create(){
			
			if(isset($_POST)){
			$pedido = new Pedido();

			$pedido->fecha_entrega = $_POST["fecha_entrega"];
			$pedido->calle = $_POST["calle"];
			$pedido->no_casa = $_POST["no_casa"];
			$pedido->localidad = $_POST["localidad"];
			$pedido->municipio = $_POST["municipio"];
			$pedido->estado = $_POST["estado"];
			$pedido->referencia = $_POST["referencia"];
			$pedido->id_cliente=$_POST["id_cliente"];
			$pedido->id_arreglo=$_POST["id_arreglo"];
			$pedido->insert();

			echo json_encode(["estatus"=>"success", "pedido"=>$pedido]);
			}
			else{
				echo "Error! revisa los campos";
			}
		}

		public function read(){
			$pedido = Pedido::selectAll();
			var_dump($pedido);
		}

		public function showid(){
			if (isset($_POST)) {
				$id_pedido=$_POST["id_pedido"];
				$pedido=Pedido::find($id_pedido);
				var_dump($pedido);
			}
			else{
				echo "Error! ingresa un id_pedido";
			}	
		}

		public function showFecha(){
			if ($_POST) {
				$fecha=$_POST["fecha"];
				$pedido=Pedido::findFecha($fecha);
				var_dump($pedido);
			}
			else{
				echo "Error! ingresa campo fecha";
			}
		}

		public function update(){
			if (isset($_POST)) {
			$pedido = new Pedido();

			$pedido->fecha_entrega = $_POST["fecha_entrega"];
			$pedido->calle = $_POST["calle"];
			$pedido->no_casa = $_POST["no_casa"];
			$pedido->localidad = $_POST["localidad"];
			$pedido->municipio = $_POST["municipio"];
			$pedido->estado = $_POST["estado"];
			$pedido->referencia = $_POST["referencia"];
			$pedido->id_cliente = $_POST["id_cliente"];
			$pedido->id_arreglo = $_POST["id_arreglo"];
			$pedido->id_pedido = $_POST["id_pedido"];
			$pedido->editar();
			echo json_encode(["estatus"=>"success", "pedido"=>$pedido]);
			}
			else{
				echo "Error! Revisa los campos";
			}
		}

		public function delete(){
			if (isset($_POST)) {
				$id_pedido=$_POST["id_pedido"];
				$pedido=Pedido::eliminar($id_pedido);
			}
			else{
				echo "No se pudo eliminar, se necesita un id";
			}
		}
	}

?>