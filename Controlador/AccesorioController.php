<?php 
use Trabajo\Modelo\Accesorio;

include "Modelo\Conexion.php";
include "Modelo\Accesorio.php";

	class AccesorioController
	{
		
		function __construct()
		{
			$this-> controller = "Accesorio";
			$this-> action = "create";
		}

		public function create(){
			
			if(isset($_POST)){
			$acs = new Accesorio();

			$acs->nom_accesorio = $_POST["nom_accesorio"];
			$acs->cantidad = $_POST["cantidad"];
			$acs->precio = $_POST["precio"];
			$acs->tipo = $_POST["tipo"];
			$acs->tamannio = $_POST["tamannio"];
			$acs->color = $_POST["color"];

			$acs->insert();

			echo json_encode(["estatus"=>"success", "accesorio"=>$acs]);
			}
			else{
				echo "Error! Revisa los campos";
			}
		}

		public function read(){
			$acs = Accesorio::selectAll();
			var_dump($acs);
		}

		public function showNom(){
			if (isset($_POST)) {
				$nom_accesorio=$_POST["nom_accesorio"];
				$acs=Accesorio::find($nom_accesorio);
				var_dump($acs);
			}
			else{
				echo "Error! ingresa campo nom_accesorio";
			}
			
		}

		public function showColor(){
			if (isset($_POST)) {
				$color=$_POST["color"];
			$acs=Accesorio::findColor($color);
			var_dump($acs);
			}
			else{
				echo "Error! ingresa campo color";
			}	
		}

		public function update(){
			if(isset($_POST)){
			$acs = new Accesorio();
			$acs->cantidad = $_POST["cantidad"];
			$acs->precio = $_POST["precio"];
			$acs->tipo = $_POST["tipo"];
			$acs->tamannio = $_POST["tamannio"];
			$acs->color = $_POST["color"];
			$acs->nom_accesorio = $_POST["nom_accesorio"];
			$acs->editar();
			echo json_encode(["estatus"=>"success", "accesorio"=>$acs]);
			}
			else{
				echo "Error! Revisa los campos";
			}
		}

		public function delete(){
			if (isset($_POST)) {
				$nom_accesorio=$_POST["nom_accesorio"];
				$acs=Accesorio::eliminar($nom_accesorio);
			}
			else{
				echo "Error! No se encontro el nombre de accesorio que desea eliminar";
			}	
		}
	}

?>