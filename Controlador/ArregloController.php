<?php 
use Trabajo\Modelo\Arreglo;

include "Modelo\Conexion.php";
include "Modelo\Arreglo.php";

	class ArregloController
	{
		
		function __construct()
		{
			$this-> controller = "Arreglo";
			$this-> action = "create";
		}

		public function create(){
			
			if(isset($_POST)){
			$arreglo = new Arreglo();

			$arreglo->nombre = $_POST["nombre"];
			$arreglo->modelo = $_POST["modelo"];
			$arreglo->descripcion = $_POST["descripcion"];
			$arreglo->precio = $_POST["precio"];
			$arreglo->nom_accesorio = $_POST["nom_accesorio"];

			$arreglo->insert();

			echo json_encode(["estatus"=>"success", "arreglo"=>$arreglo]);
			}
			else{
				echo "Error! revisa los campos";
			}
		}

		public function read(){
			$arreglo = Arreglo::selectAll();
			var_dump($arreglo);
		}

		public function showid(){
			if (isset($_POST)) {
				$id_arreglo=$_POST["id_arreglo"];
				$arreglo=Arreglo::find($id_arreglo);
				var_dump($arreglo);
			}
			else{
				echo "Error! No se encunetra id_arreglo";
			}
			
		}

		public function showNom(){
			if (isset($_POST)) {
				$nombre=$_POST["nombre"];
				$arreglo=Arreglo::findNombre($nombre);
				var_dump($arreglo);
			}
			else{
				echo "Error! No se encunetra el campo nombre";
			}
			
		}

		public function update(){
			if (isset($_POST)) {
			$arreglo = new Arreglo();

			$arreglo->nombre = $_POST["nombre"];
			$arreglo->modelo = $_POST["modelo"];
			$arreglo->descripcion = $_POST["descripcion"];
			$arreglo->precio = $_POST["precio"];
			$arreglo->nom_accesorio = $_POST["nom_accesorio"];
			$arreglo->id_arreglo = $_POST["id_arreglo"];
			$arreglo->editar();
			echo json_encode(["estatus"=>"success", "arreglo"=>$arreglo]);
			}
			else{
				echo "Error! Revisa los campos";
			}	
		}

		public function delete(){
			if (isset($_POST)) {
				$id_arreglo=$_POST["id_arreglo"];
			  	$arreglo=Arreglo::eliminar($id_arreglo); 
			  	echo "¡Registro eliminado!";
			}
			else{
				echo "No se pudo eliminar, se necesita un id";
			}	
		}
	}

?>