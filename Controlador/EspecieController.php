<?php 
use Trabajo\Modelo\Especie;

include "Modelo\Conexion.php";
include "Modelo\Especie.php";

	class EspecieController
	{
		
		function __construct()
		{
			$this-> controller = "Especie";
			$this-> action = "create";
		}

		public function create(){
			
			if(isset($_POST)){
			$especie = new Especie();

			$especie->nom_especie = $_POST["nom_especie"];
			$especie->floracion = $_POST["floracion"];

			$especie->insert();

			echo json_encode(["estatus"=>"success", "especie"=>$especie]);
			}
			else{
				echo "Error! revisa los campos";
			}
		}

		public function read(){
			$especie = Especie::selectAll();
			var_dump($especie);
		}

		public function showNom(){
			if (isset($$_POST)) {
				$nom_especie=$_POST["nom_especie"];
				$especie=Especie::find($nom_especie);
				var_dump($especie);
			}
			else{
				echo "Error! ingresa campo nombre_especie";
			}	
		}

		public function showFlora(){
			if (isset($_POST)) {
				$floracion=$_POST["floracion"];
				$especie=Especie::findNombre($floracion);
				var_dump($especie);
			}
			else{
				echo "Error! ingresa el campo floracion";
			}
			
		}

		public function update(){
			if (isset($_POST)) {
				$especie = new Especie();

				$especie->nom_especie = $_POST["nom_especie"];
				$especie->floracion = $_POST["floracion"];

				$especie->editar();
				echo json_encode(["estatus"=>"success", "especie"=>$especie]);
			}
			else{
				echo "Error! Revisa los campos";
			}
		
		}

		public function delete(){
			if (isset($_POST)) {
				$nom_especie=$_POST["nom_especie"];
				$especie=Especie::eliminar($nom_especie);
			}
			else{
				echo "No se pudo eliminar, se necesita un id";
			}
		}
	}

?>