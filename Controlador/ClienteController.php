<?php 
use Trabajo\Modelo\Cliente;

include "Modelo\Conexion.php";
include "Modelo\Cliente.php";

	class ClienteController
	{
		
		function __construct()
		{
			$this-> controller = "Cliente";
			$this-> action = "create";
		}

		public function create(){
			
			if(isset($_POST)){
			$cliente = new Cliente();

			$cliente->nombre = $_POST["nombre"];
			$cliente->apellido_p = $_POST["apellido_p"];
			$cliente->apellido_m = $_POST["apellido_m"];
			$cliente->calle = $_POST["calle"];
			$cliente->no_casa = $_POST["no_casa"];
			$cliente->localidad = $_POST["localidad"];
			$cliente->municipio = $_POST["municipio"];
			$cliente->estado = $_POST["estado"];
			$cliente->referencia = $_POST["referencia"];

			$cliente->insert();

			echo json_encode(["estatus"=>"success", "cliente"=>$cliente]);
			}
			else{
				echo "Error! revisa los campos";
			}
		}

		public function read(){
			$cliente = Cliente::selectAll();
			var_dump($cliente);
		}

		public function showid(){
			if (isset($_POST)) {
				$id_cliente=$_POST["id_cliente"];
				$cliente=Cliente::find($id_cliente);
				var_dump($cliente);
			}
			else{
				echo "Error! No se encuentra id_cliente";
			}
			
		}

		public function showNom(){
			if (isset($_POST)) {
				$nombre=$_POST["nombre"];
				$cliente=Cliente::findNombre($nombre);
				echo json_encode(["estatus"=>"success", "cliente"=>$cliente]);
			}
			else{
				echo "Error! No se encunetra el campo nombre";
			}
		}
		public function update(){
		if (isset($_POST)) {
			$cliente = new Cliente();

			$cliente->nombre = $_POST["nombre"];
			$cliente->apellido_p = $_POST["apellido_p"];
			$cliente->apellido_m = $_POST["apellido_m"];
			$cliente->calle = $_POST["calle"];
			$cliente->no_casa = $_POST["no_casa"];
			$cliente->localidad = $_POST["localidad"];
			$cliente->municipio = $_POST["municipio"];
			$cliente->estado = $_POST["estado"];
			$cliente->referencia = $_POST["referencia"];
			$cliente->id_cliente = $_POST["id_cliente"];

			$cliente->editar();
			echo json_encode(["estatus"=>"success", "cliente"=>$cliente]);
				}
		else{
			echo "Error! Revisa los campos";
		}
			
		}

		public function delete(){
			if (isset($_POST)) {
				$id_cliente=$_POST["id_cliente"];
				$cliente=Cliente::eliminar($id_cliente);
			}
			else{
				echo "No se pudo eliminar, se necesita un id";
			}
		}
	}

?>