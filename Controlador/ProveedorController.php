<?php 
use Trabajo\Modelo\Proveedor;

include "Modelo\Conexion.php";
include "Modelo\Proveedor.php";

	class ProveedorController
	{
		
		function __construct()
		{
			$this-> controller = "Proveedor";
			$this-> action = "create";
		}

		public function create(){
			
			if(isset($_POST)){
			$proveedor = new Proveedor();

			$proveedor->nombre = $_POST["nombre"];
			$proveedor->direccion = $_POST["direccion"];
			$proveedor->telefono = $_POST["telefono"];
			$proveedor->email = $_POST["email"];

			$proveedor->insert();

			echo json_encode(["estatus"=>"success", "proveedor"=>$proveedor]);
			}
			else{
				echo "Error! revisa los campos";
			}
		}

		public function read(){
			$proveedor = Proveedor::selectAll();
			var_dump($proveedor);
		}

		public function showid(){
			if (isset($_POST)) {
				$id_proveedor=$_POST["id_proveedor"];
				$proveedor=Proveedor::find($id_proveedor);
				var_dump($proveedor);
			}
			else{
				echo "Error! ingresa un id_proveedor";
			}	
		}

		public function showNom(){
			if (isset($_POST)) {
				$nombre=$_POST["nombre"];
				$proveedor=Proveedor::findNombre($nombre);
				var_dump($proveedor);
			}
			else{
				echo "Error! ingresa campo nombre";
			}	
		}
		public function update(){
			if (isset($_POST)) {
			$proveedor = new Proveedor();

			$proveedor->nombre = $_POST["nombre"];
			$proveedor->direccion = $_POST["direccion"];
			$proveedor->telefono = $_POST["telefono"];
			$proveedor->email = $_POST["email"];
			$proveedor->id_proveedor = $_POST["id_proveedor"];
			$proveedor->editar();
			echo json_encode(["estatus"=>"success", "proveedor"=>$proveedor]);
			}
			else{
				echo "Error! Revisa los campos";
			}
		}

		public function delete(){
			if (isset($_POST)) {
				$id_proveedor=$_POST["id_proveedor"];
				$proveedor=Proveedor::eliminar($id_proveedor);
			}
			else{
				echo "No se pudo eliminar, se necesita un id";
			}	
		}
	}

?>